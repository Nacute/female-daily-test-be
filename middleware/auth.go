package middleware

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Nacute/female-daily-test-be/dto"
	"net/http"
	"os"
)

func Validate() gin.HandlerFunc {
	return func(c *gin.Context) {
		key := os.Getenv("TOKEN_HEADER")
		token := c.GetHeader("Authorization")
		if token == "" {
			c.JSON(http.StatusForbidden, dto.DefaultErrorResponse("missing token", ""))
			c.Abort()
			return
		}
		if token != key {
			c.JSON(http.StatusUnauthorized, dto.DefaultErrorResponse("your token is not valid", ""))
			c.Abort()
			return
		}
		c.Next()
	}
}
