package user

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Nacute/female-daily-test-be/dto"
	"gitlab.com/Nacute/female-daily-test-be/middleware"
	"gitlab.com/Nacute/female-daily-test-be/services/reqres"
	"gitlab.com/Nacute/female-daily-test-be/utils/test_utils"
	"gorm.io/gorm"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

type rqGetListUser struct {
	ctrl   *MockControllerInterface
	enigma *middleware.MockEnigmaUtility
}

const (
	ctrlGetListUser int = iota
	ctrlGetListUserErr
	ctrlGetUserById
	ctrlGetUserByIdErrNotFound
	ctrlGetUserByIdErrDefault
	ctrlCreateUser
	valBindAndValidate
	ctrlCreateUserErr
	ctrlUpdateUserByID
	ctrlDeleteUserByID
	ctrlDeleteUserByIDErrNotFound
	ctrlDeleteUserByIDErr
)

func DefaultRqCall(mockList *rqGetListUser) map[int]test_utils.MockCall {
	return map[int]test_utils.MockCall{
		ctrlGetListUser: {
			Mock:   &mockList.ctrl.Mock,
			Method: "GetListUsers",
			Args: []any{
				context.Background(), reqres.RequestParams{
					Page: "5",
				},
			},
			Result: []any{
				&dto.Response{}, nil,
			},
			Times: 1,
		},
		ctrlGetListUserErr: {
			Mock:   &mockList.ctrl.Mock,
			Method: "GetListUsers",
			Args: []any{
				context.Background(), reqres.RequestParams{
					Page: "5",
				},
			},
			Result: []any{
				&dto.Response{}, errors.New("error"),
			},
			Times: 1,
		},
		ctrlGetUserById: {
			Mock:   &mockList.ctrl.Mock,
			Method: "GetUserById",
			Args: []any{
				context.Background(), uint(1),
			},
			Result: []any{
				&dto.Response{
					ResponseMeta: dto.ResponseMeta{Success: true},
				}, nil,
			},
			Times: 1,
		},
		ctrlGetUserByIdErrNotFound: {
			Mock:   &mockList.ctrl.Mock,
			Method: "GetUserById",
			Args: []any{
				context.Background(), uint(1),
			},
			Result: []any{
				&dto.Response{}, gorm.ErrRecordNotFound,
			},
			Times: 1,
		},
		ctrlGetUserByIdErrDefault: {
			Mock:   &mockList.ctrl.Mock,
			Method: "GetUserById",
			Args: []any{
				context.Background(), uint(1),
			},
			Result: []any{
				&dto.Response{}, errors.New("error"),
			},
			Times: 1,
		},
		ctrlCreateUser: {
			Mock:   &mockList.ctrl.Mock,
			Method: "CreateUser",
			Args: []any{
				context.Background(), CreateUserRequest{},
			},
			Result: []any{
				&dto.Response{
					ResponseMeta: dto.ResponseMeta{
						Success:      true,
						MessageTitle: "Success",
						Message:      "",
						ResponseTime: "",
					},
				}, nil,
			},
			Times: 1,
		},
		valBindAndValidate: {
			Mock:   &mockList.enigma.Mock,
			Method: "BindAndValidate",
			Result: []any{
				nil,
			},
			Times: 1,
		},
		ctrlCreateUserErr: {
			Mock:   &mockList.ctrl.Mock,
			Method: "CreateUser",
			Args: []any{
				context.Background(), CreateUserRequest{},
			},
			Result: []any{
				&dto.Response{}, errors.New("error"),
			},
			Times: 1,
		},
		ctrlUpdateUserByID: {
			Mock:   &mockList.ctrl.Mock,
			Method: "UpdateUserByID",
			Args: []any{
				context.Background(), CreateUserRequest{ID: uint(1)},
			},
			Result: []any{
				&dto.Response{
					ResponseMeta: dto.ResponseMeta{
						Success:      true,
						MessageTitle: "Success",
						Message:      "",
						ResponseTime: "",
					},
				}, nil,
			},
			Times: 1,
		},
		ctrlDeleteUserByID: {
			Mock:   &mockList.ctrl.Mock,
			Method: "DeleteUserById",
			Args: []any{
				context.Background(), uint(1),
			},
			Result: []any{
				&dto.Response{
					ResponseMeta: dto.ResponseMeta{
						Success:      true,
						MessageTitle: "Success",
						Message:      "",
						ResponseTime: "",
					},
				}, nil,
			},
			Times: 1,
		},
		ctrlDeleteUserByIDErrNotFound: {
			Mock:   &mockList.ctrl.Mock,
			Method: "DeleteUserById",
			Args: []any{
				context.Background(), uint(1),
			},
			Result: []any{
				&dto.Response{
					ResponseMeta: dto.ResponseMeta{},
				}, gorm.ErrRecordNotFound,
			},
			Times: 1,
		},
		ctrlDeleteUserByIDErr: {
			Mock:   &mockList.ctrl.Mock,
			Method: "DeleteUserById",
			Args: []any{
				context.Background(), uint(1),
			},
			Result: []any{
				&dto.Response{
					ResponseMeta: dto.ResponseMeta{},
				}, errors.New("error"),
			},
			Times: 1,
		},
	}
}

func TestRequestHandler_GetListUsers(t *testing.T) {
	gin.SetMode(gin.TestMode)
	reqMissPage, err := http.NewRequest(http.MethodGet, "/user/", nil)
	if err != nil {
		log.Println(fmt.Errorf("new request: %w", err))
		t.Fail()
		return
	}
	reqSuccess, err := http.NewRequest(http.MethodGet, "/user/?page=5", nil)
	if err != nil {
		log.Println(fmt.Errorf("new request: %w", err))
		t.Fail()
		return
	}
	tests := []struct {
		name    string
		mocks   func(*rqGetListUser)
		request *http.Request
		code    int
	}{
		{
			name: "error missing page params",
			mocks: func(user *rqGetListUser) {

			},
			request: reqMissPage,
			code:    http.StatusBadRequest,
		},
		{
			name: "error GetListUser",
			mocks: func(user *rqGetListUser) {
				mocksCall := DefaultRqCall(user)
				mcks := []test_utils.MockCall{
					mocksCall[ctrlGetListUserErr],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			request: reqSuccess,
			code:    http.StatusInternalServerError,
		},
		{
			name: "success",
			mocks: func(user *rqGetListUser) {
				mocksCall := DefaultRqCall(user)
				mcks := []test_utils.MockCall{
					mocksCall[ctrlGetListUser],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			request: reqSuccess,
			code:    http.StatusOK,
		},
	}
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			listMock := rqGetListUser{
				enigma: middleware.NewMockEnigmaUtility(t),
				ctrl:   NewMockControllerInterface(t),
			}

			if tc.mocks != nil {
				tc.mocks(&listMock)
			}

			rq := RequestHandler{
				ctrl: listMock.ctrl,
			}
			r := Router{
				rh: &rq,
			}
			_, engine := gin.CreateTestContext(httptest.NewRecorder())
			root := engine.Group("")
			r.Route(root)
			res := httptest.NewRecorder()
			engine.ServeHTTP(res, tc.request)
			assert.Equal(t, tc.code, res.Code)

		})
	}

}

func TestRequestHandler_GetUserById(t *testing.T) {
	gin.SetMode(gin.TestMode)
	req, err := http.NewRequest(http.MethodGet, "/user/1", nil)
	if err != nil {
		log.Println(fmt.Errorf("new request: %w", err))
		t.Fail()
		return
	}
	reqErrParams, err := http.NewRequest(http.MethodGet, "/user/wre3", nil)
	if err != nil {
		log.Println(fmt.Errorf("new request: %w", err))
		t.Fail()
		return
	}
	tests := []struct {
		name     string
		mocks    func(*rqGetListUser)
		request  *http.Request
		code     int
		response dto.Response
	}{
		{
			name: "success",
			mocks: func(user *rqGetListUser) {
				mocksCall := DefaultRqCall(user)
				mcks := []test_utils.MockCall{
					mocksCall[ctrlGetUserById],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			request: req,
			code:    http.StatusOK,
			response: dto.Response{
				ResponseMeta: dto.ResponseMeta{
					Success: true,
				},
			},
		},
		{
			name: "error format id",
			mocks: func(user *rqGetListUser) {
			},
			request: reqErrParams,
			code:    http.StatusBadRequest,
			response: dto.Response{
				ResponseMeta: dto.ResponseMeta{
					Success:      false,
					MessageTitle: "Oops, something went wrong.",
					Message:      "please check your id format",
				},
			},
		},
		{
			name: "error user not found",
			mocks: func(user *rqGetListUser) {
				mocksCall := DefaultRqCall(user)
				mcks := []test_utils.MockCall{
					mocksCall[ctrlGetUserByIdErrNotFound],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			request: req,
			code:    http.StatusNotFound,
			response: dto.Response{
				ResponseMeta: dto.ResponseMeta{
					Success:      false,
					MessageTitle: "Oops, something went wrong.",
					Message:      "user not found",
				},
			},
		},
		{
			name: "error GetUserById",
			mocks: func(user *rqGetListUser) {
				mocksCall := DefaultRqCall(user)
				mcks := []test_utils.MockCall{
					mocksCall[ctrlGetUserByIdErrDefault],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			request: req,
			code:    http.StatusInternalServerError,
			response: dto.Response{
				ResponseMeta: dto.ResponseMeta{
					Success:      false,
					MessageTitle: "Oops, something went wrong.",
					Message:      "error",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			listMocks := &rqGetListUser{
				enigma: middleware.NewMockEnigmaUtility(t),
				ctrl:   NewMockControllerInterface(t),
			}

			if tt.mocks != nil {
				tt.mocks(listMocks)
			}

			rh := RequestHandler{
				ctrl: listMocks.ctrl,
			}

			r := Router{
				rh: &rh,
			}

			_, engine := gin.CreateTestContext(httptest.NewRecorder())
			root := engine.Group("")
			r.Route(root)
			res := httptest.NewRecorder()
			engine.ServeHTTP(res, tt.request)

			var resMeta dto.Response
			body := res.Body.Bytes()
			err := json.Unmarshal(body, &resMeta)
			if err != nil {
				log.Println(fmt.Errorf("json.Unmarshal: %w", err))
				t.Fail()
				return
			}
			assert.Equal(t, tt.response, resMeta)
			assert.Equal(t, tt.code, res.Code)

		})
	}
}

func TestRequestHandler_CreateUser(t *testing.T) {
	type args struct {
		c *gin.Context
	}
	gin.SetMode(gin.TestMode)

	reqBody := CreateUserRequest{
		FirstName: "ridho",
		LastName:  "muhammad",
		Email:     "ridho@gmai.com",
		Avatar:    "https://i.pravatar.cc/300",
	}
	bodyByte, err := json.Marshal(reqBody)
	if err != nil {
		log.Println(fmt.Errorf("json.Marshal: %w", err))
		t.Fail()
		return
	}
	req, err := http.NewRequest(http.MethodPost, "/user/", bytes.NewReader(bodyByte))
	if err != nil {
		log.Println(fmt.Errorf("new request: %w", err))
		t.Fail()
		return
	}
	tests := []struct {
		name      string
		mocks     func(user *rqGetListUser, c *gin.Context)
		arg       args
		req       *http.Request
		code      int
		resExpect dto.Response
		resActual *httptest.ResponseRecorder
	}{
		{
			name: "success",
			mocks: func(user *rqGetListUser, c *gin.Context) {
				mocksList := DefaultRqCall(user)
				valMock := mocksList[valBindAndValidate]
				valMock.Args = []any{c, &CreateUserRequest{}}
				mcks := []test_utils.MockCall{
					valMock,
					mocksList[ctrlCreateUser],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			req:  req,
			code: 200,
			resExpect: dto.Response{
				ResponseMeta: dto.ResponseMeta{
					Success:      true,
					MessageTitle: "Success",
					Message:      "",
				},
			},
		},
		{
			name: "err ctrl.CreateUser",
			mocks: func(user *rqGetListUser, c *gin.Context) {
				mocksList := DefaultRqCall(user)
				valMock := mocksList[valBindAndValidate]
				valMock.Args = []any{c, &CreateUserRequest{}}
				mcks := []test_utils.MockCall{
					valMock,
					mocksList[ctrlCreateUserErr],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			req:  req,
			code: 500,
			resExpect: dto.Response{
				ResponseMeta: dto.ResponseMeta{
					Success:      false,
					MessageTitle: "Oops, something went wrong.",
					Message:      "error",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			listMocks := &rqGetListUser{
				ctrl:   NewMockControllerInterface(t),
				enigma: middleware.NewMockEnigmaUtility(t),
			}

			res := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(res)
			c.Request = tt.req
			tt.arg.c = c
			if tt.mocks != nil {
				tt.mocks(listMocks, c)
			}
			rh := RequestHandler{
				ctrl:   listMocks.ctrl,
				enigma: listMocks.enigma,
			}

			rh.CreateUser(tt.arg.c)

			var resMeta dto.Response
			body := res.Body.Bytes()
			err := json.Unmarshal(body, &resMeta)
			if err != nil {
				log.Println(fmt.Errorf("json.Unmarshal: %w", err))
				t.Fail()
				return
			}

			assert.Equal(t, tt.resExpect, resMeta)
			assert.Equal(t, tt.code, res.Code)

		})
	}
}

func TestRequestHandler_UpdateUserById(t *testing.T) {
	type args struct {
		c *gin.Context
	}
	gin.SetMode(gin.TestMode)

	reqBody := CreateUserRequest{
		FirstName: "ridho",
		LastName:  "muhammad",
		Email:     "ridho@gmai.com",
		Avatar:    "https://i.pravatar.cc/300",
	}
	bodyByte, err := json.Marshal(reqBody)
	if err != nil {
		log.Println(fmt.Errorf("json.Marshal: %w", err))
		t.Fail()
		return
	}
	req, err := http.NewRequest(http.MethodPost, "/user/1", bytes.NewReader(bodyByte))
	if err != nil {
		log.Println(fmt.Errorf("new request: %w", err))
		t.Fail()
		return
	}
	tests := []struct {
		name      string
		mocks     func(user *rqGetListUser, c *gin.Context)
		arg       args
		req       *http.Request
		code      int
		resExpect dto.Response
		resActual *httptest.ResponseRecorder
	}{
		{
			name: "success",
			mocks: func(user *rqGetListUser, c *gin.Context) {
				mocksList := DefaultRqCall(user)
				valMock := mocksList[valBindAndValidate]
				valMock.Args = []any{c, &CreateUserRequest{}}
				mcks := []test_utils.MockCall{
					valMock,
					mocksList[ctrlUpdateUserByID],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			req:  req,
			code: 200,
			resExpect: dto.Response{
				ResponseMeta: dto.ResponseMeta{
					Success:      true,
					MessageTitle: "Success",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			listMocks := &rqGetListUser{
				ctrl:   NewMockControllerInterface(t),
				enigma: middleware.NewMockEnigmaUtility(t),
			}

			res := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(res)
			c.Request = tt.req
			params := gin.Params{
				gin.Param{
					Key:   "id",
					Value: "1",
				},
			}
			c.Params = params
			tt.arg.c = c
			if tt.mocks != nil {
				tt.mocks(listMocks, c)
			}
			rh := RequestHandler{
				ctrl:   listMocks.ctrl,
				enigma: listMocks.enigma,
			}

			rh.UpdateUserById(tt.arg.c)

			var resMeta dto.Response
			body := res.Body.Bytes()
			err := json.Unmarshal(body, &resMeta)
			if err != nil {
				log.Println(fmt.Errorf("json.Unmarshal: %w", err))
				t.Fail()
				return
			}

			assert.Equal(t, tt.resExpect, resMeta)
			assert.Equal(t, tt.code, res.Code)

		})
	}
}

func TestRequestHandler_DeleteUserById(t *testing.T) {
	type args struct {
		c *gin.Context
	}
	gin.SetMode(gin.TestMode)

	req, err := http.NewRequest(http.MethodDelete, "/user/1", nil)
	if err != nil {
		log.Println(fmt.Errorf("new request: %w", err))
		t.Fail()
		return
	}
	tests := []struct {
		name      string
		mocks     func(user *rqGetListUser)
		arg       args
		req       *http.Request
		reqParams gin.Params
		code      int
		resExpect dto.Response
		resActual *httptest.ResponseRecorder
	}{
		{
			name: "success",
			mocks: func(user *rqGetListUser) {
				mocksList := DefaultRqCall(user)
				mcks := []test_utils.MockCall{
					mocksList[ctrlDeleteUserByID],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			reqParams: gin.Params{
				gin.Param{
					Key:   "id",
					Value: "1",
				},
			},
			req:  req,
			code: 200,
			resExpect: dto.Response{
				ResponseMeta: dto.ResponseMeta{
					Success:      true,
					MessageTitle: "Success",
				},
			},
		},
		{
			name: "error please check your id format",
			mocks: func(user *rqGetListUser) {
			},
			reqParams: gin.Params{
				gin.Param{
					Key:   "id",
					Value: "xx",
				},
			},
			req:       req,
			code:      400,
			resExpect: *dto.DefaultErrorResponse("please check your id format", ""),
		},
		{
			name: "user not found",
			mocks: func(user *rqGetListUser) {
				mocksList := DefaultRqCall(user)
				mcks := []test_utils.MockCall{
					mocksList[ctrlDeleteUserByIDErrNotFound],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			reqParams: gin.Params{
				gin.Param{
					Key:   "id",
					Value: "1",
				},
			},
			req:       req,
			code:      404,
			resExpect: *dto.DefaultErrorResponse("user not found", ""),
		},
		{
			name: "error DeleteUserById",
			mocks: func(user *rqGetListUser) {
				mocksList := DefaultRqCall(user)
				mcks := []test_utils.MockCall{
					mocksList[ctrlDeleteUserByIDErr],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			reqParams: gin.Params{
				gin.Param{
					Key:   "id",
					Value: "1",
				},
			},
			req:       req,
			code:      500,
			resExpect: *dto.DefaultErrorResponse("error", ""),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			listMocks := &rqGetListUser{
				ctrl:   NewMockControllerInterface(t),
				enigma: middleware.NewMockEnigmaUtility(t),
			}

			res := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(res)
			c.Request = tt.req
			c.Params = tt.reqParams
			tt.arg.c = c
			if tt.mocks != nil {
				tt.mocks(listMocks)
			}
			rh := RequestHandler{
				ctrl:   listMocks.ctrl,
				enigma: listMocks.enigma,
			}

			rh.DeleteUserById(tt.arg.c)

			var resMeta dto.Response
			body := res.Body.Bytes()
			err := json.Unmarshal(body, &resMeta)
			if err != nil {
				log.Println(fmt.Errorf("json.Unmarshal: %w", err))
				t.Fail()
				return
			}

			assert.Equal(t, tt.resExpect, resMeta)
			assert.Equal(t, tt.code, res.Code)

		})
	}
}
