package user

import (
	"errors"
	"fmt"
	"github.com/go-sql-driver/mysql"
	"gitlab.com/Nacute/female-daily-test-be/constant"
	"regexp"
)

func evaluateError(label string, expectedErr []error, err error) error {
	if err != nil {
		for _, errConst := range expectedErr {
			if errors.Is(err, errConst) {
				return err
			}
		}
		return fmt.Errorf("%s: %w", label, err)
	}
	return err
}

func translateSQLErr(mySqlErr *mysql.MySQLError) error {
	switch mySqlErr.Number {
	case constant.DuplicateEntryCode:
		re := regexp.MustCompile(`for key '([^']+)`)
		msgs := re.FindStringSubmatch(mySqlErr.Message)
		if len(msgs) > 1 && msgs[1] == "users.users_pk_email" {
			return constant.ErrUserExist
		}
	}

	return mySqlErr

}
