package user

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Nacute/female-daily-test-be/middleware"
	"gitlab.com/Nacute/female-daily-test-be/repository"
	"gitlab.com/Nacute/female-daily-test-be/services/reqres"
)

type Router struct {
	rh *RequestHandler
}

func NewRouter(
	reqres reqres.Services,
	userRepo repository.UserRepoInterface,
	enigma middleware.EnigmaUtility,
) Router {
	return Router{
		rh: &RequestHandler{
			ctrl: &Controller{
				uc: &Usecase{
					reqres:   reqres,
					userRepo: userRepo,
				},
			},
			enigma: enigma,
		},
	}
}

func (r Router) Route(handler *gin.RouterGroup) {
	userRoute := handler.Group("/user")
	userRoute.GET("/", r.rh.GetListUsers)
	userRoute.POST("/", r.rh.CreateUser)
	userRoute.DELETE("/:id", middleware.Validate(), r.rh.DeleteUserById)
	userRoute.GET("/:id", r.rh.GetUserById)
	userRoute.PUT("/:id", r.rh.UpdateUserById)
}
