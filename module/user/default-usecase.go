//go:generate mockery --all --inpackage --case snake

package user

import (
	"context"
	"errors"
	"fmt"
	"github.com/go-sql-driver/mysql"
	"gitlab.com/Nacute/female-daily-test-be/constant"
	"gitlab.com/Nacute/female-daily-test-be/entities"
	"gitlab.com/Nacute/female-daily-test-be/repository"
	"gitlab.com/Nacute/female-daily-test-be/services/reqres"
	"strings"
)

type Usecase struct {
	reqres   reqres.Services
	userRepo repository.UserRepoInterface
}

type UsecaseInterface interface {
	GetUserById(ctx context.Context, id uint) (Item, error)
	GetListUsers(
		ctx context.Context,
		payload reqres.RequestParams,
	) ([]reqres.ResponseItem, error)
	CreateUser(
		ctx context.Context,
		payload *entities.Users,
	) (*entities.Users, error)
	UpdateUserByID(
		ctx context.Context,
		payload CreateUserRequest,
	) (*entities.Users, error)
	DeleteUserById(ctx context.Context, id uint) error
}

func (uc Usecase) DeleteUserById(
	ctx context.Context,
	id uint,
) error {
	found, err := uc.userRepo.IsUserExist(ctx, id)
	if err != nil {
		return fmt.Errorf("userRepo.IsUserExist: %w", err)
	}
	if !found {
		return constant.ErrUserNotFound
	}
	err = uc.userRepo.DeleteByID(ctx, id)
	if err != nil {
		return fmt.Errorf("userRepo.DeleteByID: %w", err)
	}

	return nil
}

func (uc Usecase) UpdateUserByID(
	ctx context.Context,
	payload CreateUserRequest,
) (*entities.Users, error) {
	found, err := uc.userRepo.IsUserExist(ctx, payload.ID)
	if err != nil {
		return &entities.Users{}, fmt.Errorf("userRepo.IsUserExist: %w", err)
	}
	if !found {
		return &entities.Users{}, constant.ErrUserNotFound
	}
	user, err := uc.userRepo.Update(
		ctx,
		&entities.Users{
			ID:        payload.ID,
			FirstName: strings.Title(payload.FirstName),
			LastName:  strings.Title(payload.LastName),
			Email:     strings.ToLower(payload.Email),
			Avatar:    payload.Avatar,
		})
	if err != nil {
		var mySqlErr *mysql.MySQLError
		if errors.As(err, &mySqlErr) {
			err := translateSQLErr(mySqlErr)
			return nil, err
		}
		return &entities.Users{}, fmt.Errorf("userRepo.Update: %w", err)
	}

	return user, nil
}

func (uc Usecase) CreateUser(
	ctx context.Context,
	user *entities.Users,
) (*entities.Users, error) {
	user, err := uc.userRepo.Store(ctx, user)
	if err != nil {
		var mySqlErr *mysql.MySQLError
		if errors.As(err, &mySqlErr) {
			err := translateSQLErr(mySqlErr)
			return nil, err
		}
		return nil, fmt.Errorf("userRepo.Store: %w", err)
	}
	return user, nil

}

func (uc Usecase) GetUserById(ctx context.Context, id uint) (Item, error) {
	user, err := uc.userRepo.GetByID(ctx, id)
	if err != nil {
		return Item{}, err
	}
	return Item{
		ID:        user.ID,
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
		Avatar:    user.Avatar,
	}, nil
}

func (uc Usecase) GetListUsers(
	ctx context.Context,
	payload reqres.RequestParams,
) ([]reqres.ResponseItem, error) {
	reqresRes, err := uc.reqres.GetListUser(ctx, payload)
	if err != nil {
		return nil, fmt.Errorf("reqres.GetListUser: %w", err)
	}
	users := uc.parseToUserModel(reqresRes.Data)
	users, err = uc.userRepo.StoreDatas(ctx, users)
	if err != nil {
		return nil, fmt.Errorf("userRepo.StoreDatas: %w", err)
	}

	return reqresRes.Data, nil
}

func (uc Usecase) parseToUserModel(res []reqres.ResponseItem) []entities.Users {
	var users = make([]entities.Users, len(res))
	for i, _ := range res {
		users[i] = entities.Users{
			ID:        uint(res[i].Id),
			FirstName: strings.Title(res[i].FirstName),
			LastName:  strings.Title(res[i].LastName),
			Email:     strings.ToLower(res[i].Email),
			Avatar:    res[i].Avatar,
		}
	}

	return users
}
