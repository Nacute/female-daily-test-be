package user

import (
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.com/Nacute/female-daily-test-be/dto"
	"gitlab.com/Nacute/female-daily-test-be/middleware"
	"gitlab.com/Nacute/female-daily-test-be/services/reqres"
	"gorm.io/gorm"
	"net/http"
	"strconv"
)

type RequestHandler struct {
	ctrl   ControllerInterface
	enigma middleware.EnigmaUtility
}

func (rh RequestHandler) CreateUser(c *gin.Context) {
	var payload = CreateUserRequest{}
	if errs := rh.enigma.BindAndValidate(c, &payload); len(errs) > 0 {
		c.JSON(http.StatusBadRequest, dto.DefaultInvalidInputResponse(errs))
		return
	}

	res, err := rh.ctrl.CreateUser(c.Request.Context(), payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.DefaultErrorResponse(err.Error(), ""))
		return
	}

	c.JSON(http.StatusOK, res)

}

func (rh RequestHandler) GetUserById(c *gin.Context) {
	id, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		var errSyntax *strconv.NumError
		if errors.As(err, &errSyntax) {
			c.JSON(http.StatusBadRequest, dto.DefaultErrorResponse("please check your id format", ""))
			return
		}
		c.JSON(http.StatusBadRequest, dto.DefaultErrorResponse(err.Error(), ""))
		return
	}

	res, err := rh.ctrl.GetUserById(c.Request.Context(), uint(id))
	if err != nil {
		if errors.Is(gorm.ErrRecordNotFound, err) {
			c.JSON(http.StatusNotFound, dto.DefaultErrorResponse("user not found", ""))
			return
		}
		c.JSON(http.StatusInternalServerError, dto.DefaultErrorResponse(err.Error(), ""))
		return
	}

	c.JSON(http.StatusOK, res)

}

func (rh RequestHandler) GetListUsers(c *gin.Context) {
	var params reqres.RequestParams
	page := c.Query("page")
	perPage := c.Query("per_page")
	if page == "" {
		c.JSON(http.StatusBadRequest, dto.DefaultErrorResponse("missing page's query params", ""))
		return
	}
	params.Page = page
	params.PerPage = perPage
	res, err := rh.ctrl.GetListUsers(c.Request.Context(), params)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.DefaultErrorResponse(err.Error(), ""))
		return
	}

	c.JSON(http.StatusOK, res)
}

func (rh RequestHandler) UpdateUserById(c *gin.Context) {
	id, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		var errSyntax *strconv.NumError
		if errors.As(err, &errSyntax) {
			c.JSON(http.StatusBadRequest, dto.DefaultErrorResponse("please check your id format", ""))
			return
		}
		c.JSON(http.StatusBadRequest, dto.DefaultErrorResponse(err.Error(), ""))
		return
	}
	var payload CreateUserRequest
	if errs := rh.enigma.BindAndValidate(c, &payload); len(errs) > 0 {
		c.JSON(http.StatusBadRequest, dto.DefaultInvalidInputResponse(errs))
		return
	}
	payload.ID = uint(id)

	res, err := rh.ctrl.UpdateUserByID(c.Request.Context(), payload)
	if err != nil {
		c.JSON(http.StatusInternalServerError, dto.DefaultErrorResponse(err.Error(), ""))
		return
	}

	c.JSON(http.StatusOK, res)
}

func (rh RequestHandler) DeleteUserById(c *gin.Context) {
	id, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		var errSyntax *strconv.NumError
		if errors.As(err, &errSyntax) {
			c.JSON(http.StatusBadRequest, dto.DefaultErrorResponse("please check your id format", ""))
			return
		}
		c.JSON(http.StatusBadRequest, dto.DefaultErrorResponse(err.Error(), ""))
		return
	}

	res, err := rh.ctrl.DeleteUserById(c.Request.Context(), uint(id))
	if err != nil {
		if errors.Is(gorm.ErrRecordNotFound, err) {
			c.JSON(http.StatusNotFound, dto.DefaultErrorResponse("user not found", ""))
			return
		}
		c.JSON(http.StatusInternalServerError, dto.DefaultErrorResponse(err.Error(), ""))
		return
	}

	c.JSON(http.StatusOK, res)
}
