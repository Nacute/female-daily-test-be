//go:generate mockery --all --inpackage --case snake

package user

import (
	"context"
	"fmt"
	"gitlab.com/Nacute/female-daily-test-be/constant"
	"gitlab.com/Nacute/female-daily-test-be/dto"
	"gitlab.com/Nacute/female-daily-test-be/entities"
	"gitlab.com/Nacute/female-daily-test-be/services/reqres"
	"gorm.io/gorm"
	"strings"
	"time"
)

type Controller struct {
	uc UsecaseInterface
}

type ControllerInterface interface {
	GetListUsers(
		ctx context.Context,
		params reqres.RequestParams,
	) (*dto.Response, error)
	GetUserById(
		ctx context.Context,
		id uint,
	) (*dto.Response, error)
	CreateUser(
		ctx context.Context,
		payload CreateUserRequest,
	) (*dto.Response, error)
	UpdateUserByID(
		ctx context.Context,
		payload CreateUserRequest,
	) (*dto.Response, error)
	DeleteUserById(ctx context.Context, id uint) (*dto.Response, error)
}

func (c Controller) DeleteUserById(ctx context.Context, id uint) (*dto.Response, error) {
	start := time.Now()
	err := c.uc.DeleteUserById(ctx, id)
	err = evaluateError("uc.DeleteUserById", []error{
		constant.ErrUserNotFound,
	}, err)
	if err != nil {
		return &dto.Response{}, err
	}

	return dto.NewSuccessResponse(
		nil,
		"user deleted successfully",
		fmt.Sprint(time.Since(start).Milliseconds(), ".ms"),
	), nil
}

func (c Controller) UpdateUserByID(
	ctx context.Context,
	payload CreateUserRequest,
) (*dto.Response, error) {
	start := time.Now()
	result, err := c.uc.UpdateUserByID(ctx, payload)
	err = evaluateError("uc.UpdateUserByID", []error{
		constant.ErrUserExist,
		constant.ErrUserNotFound,
	}, err)
	if err != nil {
		return &dto.Response{}, err
	}

	return dto.NewSuccessResponse(
		result,
		"user updated successfully",
		fmt.Sprint(time.Since(start).Milliseconds(), ".ms"),
	), nil

}

func (c Controller) CreateUser(
	ctx context.Context,
	payload CreateUserRequest,
) (*dto.Response, error) {
	start := time.Now()
	result, err := c.uc.CreateUser(
		ctx,
		&entities.Users{
			FirstName: strings.Title(payload.FirstName),
			LastName:  strings.Title(payload.LastName),
			Email:     strings.ToLower(payload.Email),
			Avatar:    payload.Avatar,
		})
	err = evaluateError("uc.CreateUser", []error{
		constant.ErrUserExist,
	}, err)
	if err != nil {
		return &dto.Response{}, err
	}

	return dto.NewSuccessResponse(
		result,
		"user has been created successfully",
		fmt.Sprint(time.Since(start).Milliseconds(), ".ms"),
	), nil
}

func (c Controller) GetListUsers(
	ctx context.Context,
	params reqres.RequestParams,
) (*dto.Response, error) {
	start := time.Now()
	data, err := c.uc.GetListUsers(ctx, params)
	if err != nil {
		return &dto.Response{}, fmt.Errorf("uc.GetListUsers: %w", err)
	}

	return dto.NewSuccessResponse(
		data,
		"list user retrieved successfully",
		fmt.Sprint(time.Since(start).Milliseconds(), " ms."),
	), nil
}

func (c Controller) GetUserById(
	ctx context.Context,
	id uint,
) (*dto.Response, error) {
	start := time.Now()
	data, err := c.uc.GetUserById(ctx, id)
	err = evaluateError("uc.GetUserById", []error{gorm.ErrRecordNotFound}, err)
	if err != nil {
		return &dto.Response{}, err
	}

	return dto.NewSuccessResponse(
		data,
		"user retrieved successfully",
		fmt.Sprint(time.Since(start).Milliseconds(), " ms."),
	), nil
}
