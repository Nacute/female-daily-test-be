package user

import "gitlab.com/Nacute/female-daily-test-be/dto"

type Item struct {
	ID        uint   `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
	Avatar    string `json:"avatar"`
}

type CreateUserRequest struct {
	ID        uint   `json:"-"`
	FirstName string `json:"firstName" validate:"alpha"`
	LastName  string `json:"lastName" validate:"alpha"`
	Email     string `json:"email" validate:"email"`
	Avatar    string `json:"avatar" validate:"url"`
}

type ResponseCreateUser struct {
	dto.ResponseMeta
	Data CreateUserRequest `json:"data"`
}
