package user

import (
	"context"
	"errors"
	"fmt"
	"github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Nacute/female-daily-test-be/constant"
	"gitlab.com/Nacute/female-daily-test-be/entities"
	"gitlab.com/Nacute/female-daily-test-be/repository"
	"gitlab.com/Nacute/female-daily-test-be/services/reqres"
	"gitlab.com/Nacute/female-daily-test-be/utils/test_utils"
	"gorm.io/gorm"
	"reflect"
	"testing"
)

type GetListUserMocks struct {
	userRepoMocks *repository.MockUserRepoInterface
	reqresMocks   *reqres.MockServices
}

const (
	userRepoStoreDatasTrue int = iota
	reqresGetListUserTrue
	reqresGetListUserFalse
	userRepoStoreDatasFalse
	userRepoGetByIdFalse
	userRepoGetById
	userRepoStore
	userRepoStoreErr
	userRepoStoreErrUserExist
	userRepoIsUserExist
	userRepoUpdate
	userRepoIsUserExistErr
	userRepoIsUserExistNotFound
	userRepoUpdateErrDuplicate
	userRepoDeleteByID
	userRepoDeleteByIDNotFound
	userRepoDeleteByIDErr
)

func DefaultCall(mocks *GetListUserMocks) map[int]test_utils.MockCall {
	return map[int]test_utils.MockCall{
		userRepoStoreDatasTrue: {
			Mock:   &mocks.userRepoMocks.Mock,
			Method: "StoreDatas",
			Args: []any{
				context.Background(),
				[]entities.Users{},
			},
			Result: []any{
				[]entities.Users{},
				nil,
			},
			Times: 1,
		},
		reqresGetListUserTrue: {
			Mock:   &mocks.reqresMocks.Mock,
			Method: "GetListUser",
			Args: []any{
				context.Background(),
				reqres.RequestParams{},
			},
			Result: []any{
				reqres.GetListUsereResponse{
					Data: []reqres.ResponseItem{},
				},
				nil,
			},
			Times: 1,
		},
		reqresGetListUserFalse: {
			Mock:   &mocks.reqresMocks.Mock,
			Method: "GetListUser",
			Args: []any{
				context.Background(),
				reqres.RequestParams{},
			},
			Result: []any{
				reqres.GetListUsereResponse{},
				errors.New("error"),
			},
			Times: 1,
		},
		userRepoStoreDatasFalse: {
			Mock:   &mocks.userRepoMocks.Mock,
			Method: "StoreDatas",
			Args: []any{
				context.Background(),
				[]entities.Users{},
			},
			Result: []any{
				nil,
				errors.New("error"),
			},
			Times: 1,
		},
		userRepoGetByIdFalse: {
			Mock:   &mocks.userRepoMocks.Mock,
			Method: "GetByID",
			Args: []any{
				context.Background(),
				uint(1),
			},
			Result: []any{
				entities.Users{},
				errors.New("error"),
			},
			Times: 1,
		},
		userRepoGetById: {
			Mock:   &mocks.userRepoMocks.Mock,
			Method: "GetByID",
			Args: []any{
				context.Background(),
				uint(1),
			},
			Result: []any{
				entities.Users{
					ID: uint(1),
				},
				nil,
			},
			Times: 1,
		},
		userRepoStore: {
			Mock:   &mocks.userRepoMocks.Mock,
			Method: "Store",
			Args: []any{
				context.Background(),
				&entities.Users{ID: 1},
			},
			Result: []any{
				&entities.Users{
					ID: uint(1),
				},
				nil,
			},
			Times: 1,
		},
		userRepoStoreErr: {
			Mock:   &mocks.userRepoMocks.Mock,
			Method: "Store",
			Args: []any{
				context.Background(),
				&entities.Users{ID: 1},
			},
			Result: []any{
				nil,
				errors.New("error"),
			},
			Times: 1,
		},
		userRepoStoreErrUserExist: {
			Mock:   &mocks.userRepoMocks.Mock,
			Method: "Store",
			Args: []any{
				context.Background(),
				&entities.Users{ID: 1},
			},
			Result: []any{
				nil,
				&mysql.MySQLError{
					Number:  constant.DuplicateEntryCode,
					Message: "Duplicate entry 'users.users_pk_email@gmail.com' for key 'users.users_pk_email'",
				},
			},
			Times: 1,
		},
		userRepoIsUserExist: {
			Mock:   &mocks.userRepoMocks.Mock,
			Method: "IsUserExist",
			Args: []any{
				context.Background(),
				uint(1),
			},
			Result: []any{
				true,
				nil,
			},
			Times: 1,
		},
		userRepoUpdate: {
			Mock:   &mocks.userRepoMocks.Mock,
			Method: "Update",
			Args: []any{
				context.Background(),
				&entities.Users{ID: 1},
			},
			Result: []any{
				&entities.Users{ID: 1},
				nil,
			},
			Times: 1,
		},
		userRepoIsUserExistErr: {
			Mock:   &mocks.userRepoMocks.Mock,
			Method: "IsUserExist",
			Args: []any{
				context.Background(),
				uint(1),
			},
			Result: []any{
				false,
				errors.New("error"),
			},
			Times: 1,
		},
		userRepoIsUserExistNotFound: {
			Mock:   &mocks.userRepoMocks.Mock,
			Method: "IsUserExist",
			Args: []any{
				context.Background(),
				uint(1),
			},
			Result: []any{
				false,
				nil,
			},
			Times: 1,
		},
		userRepoUpdateErrDuplicate: {
			Mock:   &mocks.userRepoMocks.Mock,
			Method: "Update",
			Args: []any{
				context.Background(),
				&entities.Users{ID: 1},
			},
			Result: []any{
				nil,
				&mysql.MySQLError{
					Number:  constant.DuplicateEntryCode,
					Message: "Duplicate entry 'users.users_pk_email@gmail.com' for key 'users.users_pk_email'",
				},
			},
			Times: 1,
		},
		userRepoDeleteByID: {
			Mock:   &mocks.userRepoMocks.Mock,
			Method: "DeleteByID",
			Args: []any{
				context.Background(),
				uint(1),
			},
			Result: []any{
				nil,
			},
			Times: 1,
		},
		userRepoDeleteByIDNotFound: {
			Mock:   &mocks.userRepoMocks.Mock,
			Method: "DeleteByID",
			Args: []any{
				context.Background(),
				uint(1),
			},
			Result: []any{
				gorm.ErrRecordNotFound,
			},
			Times: 1,
		},
		userRepoDeleteByIDErr: {
			Mock:   &mocks.userRepoMocks.Mock,
			Method: "DeleteByID",
			Args: []any{
				context.Background(),
				uint(1),
			},
			Result: []any{
				errors.New("error"),
			},
			Times: 1,
		},
	}
}

func TestUsecase_GetListUsers(t *testing.T) {
	testCases := []struct {
		name    string
		mocks   func(*GetListUserMocks)
		params  reqres.RequestParams
		want    []reqres.ResponseItem
		wantErr error
	}{
		{
			name: "success",
			mocks: func(mocks *GetListUserMocks) {
				mckCall := DefaultCall(mocks)
				mcks := []test_utils.MockCall{
					mckCall[userRepoStoreDatasTrue],
					mckCall[reqresGetListUserTrue],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			params:  reqres.RequestParams{},
			want:    []reqres.ResponseItem{},
			wantErr: nil,
		},
		{
			name: "failed GetList",
			mocks: func(mocks *GetListUserMocks) {
				mckCall := DefaultCall(mocks)
				mcks := []test_utils.MockCall{mckCall[reqresGetListUserFalse]}
				test_utils.GenerateMockMethod(mcks)
			},
			params:  reqres.RequestParams{},
			want:    nil,
			wantErr: errors.New("reqres.GetListUser: error"),
		},
		{
			name: "failed StoreDatas",
			mocks: func(mocks *GetListUserMocks) {
				mckCall := DefaultCall(mocks)
				mcks := []test_utils.MockCall{
					mckCall[reqresGetListUserTrue],
					mckCall[userRepoStoreDatasFalse],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			params:  reqres.RequestParams{},
			want:    nil,
			wantErr: errors.New("userRepo.StoreDatas: error"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			listMock := GetListUserMocks{
				userRepoMocks: repository.NewMockUserRepoInterface(t),
				reqresMocks:   reqres.NewMockServices(t),
			}

			if tc.mocks != nil {
				tc.mocks(&listMock)
			}

			uc := Usecase{
				reqres:   listMock.reqresMocks,
				userRepo: listMock.userRepoMocks,
			}

			result, err := uc.GetListUsers(context.Background(), tc.params)
			if err == nil {
				assert.Equal(t, tc.wantErr, err)
			} else {
				assert.Equal(t, tc.wantErr.Error(), err.Error())
			}

			if !reflect.DeepEqual(result, tc.want) {
				t.Errorf("GetListUsers() got = %v, want %v", result, tc.want)
			}

			listMock.userRepoMocks.AssertExpectations(t)
			listMock.reqresMocks.AssertExpectations(t)

		})
	}
}

func TestUsecase_GetUserById(t *testing.T) {
	type args struct {
		ctx context.Context
		id  uint
	}
	tests := []struct {
		name    string
		mocks   func(mocks *GetListUserMocks)
		args    args
		want    Item
		wantErr error
	}{
		{
			name: "error userRepo.GetByID",
			mocks: func(mocks *GetListUserMocks) {
				mocksList := DefaultCall(mocks)
				mcks := []test_utils.MockCall{
					mocksList[userRepoGetByIdFalse],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx: context.Background(),
				id:  1,
			},
			want:    Item{},
			wantErr: errors.New("error"),
		},
		{
			name: "success",
			mocks: func(mocks *GetListUserMocks) {
				mocksList := DefaultCall(mocks)
				mcks := []test_utils.MockCall{
					mocksList[userRepoGetById],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx: context.Background(),
				id:  1,
			},
			want: Item{
				ID: uint(1),
			},
			wantErr: nil,
		},
	}
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			listMock := GetListUserMocks{
				userRepoMocks: repository.NewMockUserRepoInterface(t),
				reqresMocks:   reqres.NewMockServices(t),
			}

			if tc.mocks != nil {
				tc.mocks(&listMock)
			}

			uc := Usecase{
				reqres:   listMock.reqresMocks,
				userRepo: listMock.userRepoMocks,
			}
			result, err := uc.GetUserById(tc.args.ctx, tc.args.id)
			if err == nil {
				assert.Equal(t, tc.wantErr, err)
			} else {
				assert.Equal(t, tc.wantErr.Error(), err.Error())
			}

			if !reflect.DeepEqual(result, tc.want) {
				t.Errorf("GetUserById() got = %v, want %v", result, tc.want)
			}
			listMock.userRepoMocks.AssertExpectations(t)
			listMock.reqresMocks.AssertExpectations(t)
		})
	}
}

func TestUsecase_CreateUser(t *testing.T) {
	type args struct {
		ctx  context.Context
		user *entities.Users
	}
	tests := []struct {
		name    string
		mocks   func(*GetListUserMocks)
		args    args
		want    *entities.Users
		wantErr error
	}{
		{
			name: "success",
			mocks: func(mocks *GetListUserMocks) {
				mockCalls := DefaultCall(mocks)
				mcks := []test_utils.MockCall{
					mockCalls[userRepoStore],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx:  context.Background(),
				user: &entities.Users{ID: 1},
			},
			want:    &entities.Users{ID: 1},
			wantErr: nil,
		},
		{
			name: "err userRepo.Store",
			mocks: func(mocks *GetListUserMocks) {
				mockCalls := DefaultCall(mocks)
				mcks := []test_utils.MockCall{
					mockCalls[userRepoStoreErr],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx:  context.Background(),
				user: &entities.Users{ID: 1},
			},
			want:    nil,
			wantErr: fmt.Errorf("userRepo.Store: error"),
		},
		{
			name: "err email already used",
			mocks: func(mocks *GetListUserMocks) {
				mockCalls := DefaultCall(mocks)
				mcks := []test_utils.MockCall{
					mockCalls[userRepoStoreErrUserExist],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx:  context.Background(),
				user: &entities.Users{ID: 1},
			},
			want:    nil,
			wantErr: constant.ErrUserExist,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			listMocks := &GetListUserMocks{
				userRepoMocks: repository.NewMockUserRepoInterface(t),
				reqresMocks:   reqres.NewMockServices(t),
			}

			if tt.mocks != nil {
				tt.mocks(listMocks)
			}

			uc := Usecase{
				reqres:   listMocks.reqresMocks,
				userRepo: listMocks.userRepoMocks,
			}
			got, err := uc.CreateUser(tt.args.ctx, tt.args.user)
			if err == nil {
				assert.Equal(t, tt.wantErr, err)
			} else {
				assert.Equal(t, tt.wantErr.Error(), err.Error())
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateUser() got = %v, want %v", got, tt.want)
			}
			listMocks.userRepoMocks.AssertExpectations(t)
			listMocks.reqresMocks.AssertExpectations(t)
		})
	}
}

func TestUsecase_UpdateUserByID(t *testing.T) {
	type fields struct {
		reqres   reqres.Services
		userRepo repository.UserRepoInterface
	}
	type args struct {
		ctx     context.Context
		payload CreateUserRequest
	}
	tests := []struct {
		name    string
		mocks   func(*GetListUserMocks)
		args    args
		want    *entities.Users
		wantErr error
	}{
		{
			name: "success",
			mocks: func(mocks *GetListUserMocks) {
				mocksCall := DefaultCall(mocks)
				mcks := []test_utils.MockCall{
					mocksCall[userRepoIsUserExist],
					mocksCall[userRepoUpdate],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx:     context.Background(),
				payload: CreateUserRequest{ID: 1},
			},
			want:    &entities.Users{ID: 1},
			wantErr: nil,
		},
		{
			name: "error userRepo.IsUserExist",
			mocks: func(mocks *GetListUserMocks) {
				mocksCall := DefaultCall(mocks)
				mcks := []test_utils.MockCall{
					mocksCall[userRepoIsUserExistErr],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx:     context.Background(),
				payload: CreateUserRequest{ID: 1},
			},
			want:    &entities.Users{},
			wantErr: errors.New("userRepo.IsUserExist: error"),
		},
		{
			name: "error user not found",
			mocks: func(mocks *GetListUserMocks) {
				mocksCall := DefaultCall(mocks)
				mcks := []test_utils.MockCall{
					mocksCall[userRepoIsUserExistNotFound],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx:     context.Background(),
				payload: CreateUserRequest{ID: 1},
			},
			want:    &entities.Users{},
			wantErr: constant.ErrUserNotFound,
		},
		{
			name: "error userRepo.Update",
			mocks: func(mocks *GetListUserMocks) {
				mocksCall := DefaultCall(mocks)
				mcks := []test_utils.MockCall{
					mocksCall[userRepoIsUserExist],
					mocksCall[userRepoUpdateErrDuplicate],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx:     context.Background(),
				payload: CreateUserRequest{ID: 1},
			},
			want:    nil,
			wantErr: constant.ErrUserExist,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			listMock := &GetListUserMocks{
				userRepoMocks: repository.NewMockUserRepoInterface(t),
				reqresMocks:   reqres.NewMockServices(t),
			}

			if tt.mocks != nil {
				tt.mocks(listMock)
			}

			uc := Usecase{
				reqres:   listMock.reqresMocks,
				userRepo: listMock.userRepoMocks,
			}
			got, err := uc.UpdateUserByID(tt.args.ctx, tt.args.payload)
			if err == nil {
				assert.Equal(t, tt.wantErr, err)
			} else {
				assert.Equal(t, tt.wantErr.Error(), err.Error())
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateUserByID() got = %v, want %v", got, tt.want)
			}
			listMock.userRepoMocks.AssertExpectations(t)
			listMock.reqresMocks.AssertExpectations(t)
		})
	}
}

func TestUsecase_DeleteUserById(t *testing.T) {
	type args struct {
		ctx context.Context
		id  uint
	}
	tests := []struct {
		name    string
		mocks   func(mocks *GetListUserMocks)
		args    args
		wantErr error
	}{
		{
			name: "success",
			mocks: func(mocks *GetListUserMocks) {
				listMocks := DefaultCall(mocks)
				mcks := []test_utils.MockCall{
					listMocks[userRepoIsUserExist],
					listMocks[userRepoDeleteByID],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx: context.Background(),
				id:  uint(1),
			},
			wantErr: nil,
		},
		{
			name: "error user not found",
			mocks: func(mocks *GetListUserMocks) {
				listMocks := DefaultCall(mocks)
				mcks := []test_utils.MockCall{
					listMocks[userRepoIsUserExistNotFound],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx: context.Background(),
				id:  uint(1),
			},
			wantErr: constant.ErrUserNotFound,
		},
		{
			name: "userRepo.DeleteByID",
			mocks: func(mocks *GetListUserMocks) {
				listMocks := DefaultCall(mocks)
				mcks := []test_utils.MockCall{
					listMocks[userRepoIsUserExist],
					listMocks[userRepoDeleteByIDErr],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx: context.Background(),
				id:  uint(1),
			},
			wantErr: fmt.Errorf("userRepo.DeleteByID: error"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			listMock := &GetListUserMocks{
				userRepoMocks: repository.NewMockUserRepoInterface(t),
				reqresMocks:   reqres.NewMockServices(t),
			}

			if tt.mocks != nil {
				tt.mocks(listMock)
			}

			uc := Usecase{
				reqres:   listMock.reqresMocks,
				userRepo: listMock.userRepoMocks,
			}
			err := uc.DeleteUserById(tt.args.ctx, tt.args.id)
			if err == nil {
				assert.Equal(t, tt.wantErr, err)
			} else {
				assert.Equal(t, tt.wantErr.Error(), err.Error())
			}
			listMock.userRepoMocks.AssertExpectations(t)
			listMock.reqresMocks.AssertExpectations(t)
		})
	}
}
