package user

import (
	"context"
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Nacute/female-daily-test-be/dto"
	"gitlab.com/Nacute/female-daily-test-be/entities"
	"gitlab.com/Nacute/female-daily-test-be/services/reqres"
	"gitlab.com/Nacute/female-daily-test-be/utils/test_utils"
	"gorm.io/gorm"
	"reflect"
	"testing"
)

type CtrlGetListUser struct {
	uc *MockUsecaseInterface
}

const (
	ucGetlistUserErr int = iota
	ucGetlistUsers
	ucGetByID
	ucGetByIDErrNotFound
	ucGetByIDErrDefault
	ucCreateUser
	ucCreateUserErr
	ucUpdateUserByID
	ucDeleteUserByID
)

func DefaultCtrlCall(mocklist *CtrlGetListUser) map[int]test_utils.MockCall {
	return map[int]test_utils.MockCall{
		ucGetlistUserErr: {
			Mock:   &mocklist.uc.Mock,
			Method: "GetListUsers",
			Args: []any{
				context.Background(),
				reqres.RequestParams{},
			},
			Result: []any{
				nil,
				errors.New("error"),
			},
			Times: 1,
		},
		ucGetlistUsers: {
			Mock:   &mocklist.uc.Mock,
			Method: "GetListUsers",
			Args: []any{
				context.Background(),
				reqres.RequestParams{},
			},
			Result: []any{
				[]reqres.ResponseItem{},
				nil,
			},
			Times: 1,
		},
		ucGetByID: {
			Mock:   &mocklist.uc.Mock,
			Method: "GetUserById",
			Args: []any{
				context.Background(),
				uint(1),
			},
			Result: []any{
				Item{ID: 1},
				nil,
			},
			Times: 1,
		},
		ucGetByIDErrNotFound: {
			Mock:   &mocklist.uc.Mock,
			Method: "GetUserById",
			Args: []any{
				context.Background(),
				uint(1),
			},
			Result: []any{
				Item{},
				gorm.ErrRecordNotFound,
			},
			Times: 1,
		},
		ucGetByIDErrDefault: {
			Mock:   &mocklist.uc.Mock,
			Method: "GetUserById",
			Args: []any{
				context.Background(),
				uint(1),
			},
			Result: []any{
				Item{},
				errors.New("error"),
			},
			Times: 1,
		},
		ucCreateUser: {
			Mock:   &mocklist.uc.Mock,
			Method: "CreateUser",
			Args: []any{
				context.Background(),
				&entities.Users{},
			},
			Result: []any{
				&entities.Users{},
				nil,
			},
			Times: 1,
		},
		ucCreateUserErr: {
			Mock:   &mocklist.uc.Mock,
			Method: "CreateUser",
			Args: []any{
				context.Background(),
				&entities.Users{},
			},
			Result: []any{
				nil,
				errors.New("error"),
			},
			Times: 1,
		},
		ucUpdateUserByID: {
			Mock:   &mocklist.uc.Mock,
			Method: "UpdateUserByID",
			Args: []any{
				context.Background(),
				CreateUserRequest{},
			},
			Result: []any{
				&entities.Users{},
				nil,
			},
			Times: 1,
		},
		ucDeleteUserByID: {
			Mock:   &mocklist.uc.Mock,
			Method: "DeleteUserById",
			Args: []any{
				context.Background(),
				uint(1),
			},
			Result: []any{
				nil,
			},
			Times: 1,
		},
	}
}

func TestController_GetListUsers(t *testing.T) {
	type args struct {
		ctx    context.Context
		params reqres.RequestParams
	}
	tests := []struct {
		name    string
		mocks   func(*CtrlGetListUser)
		args    args
		want    *dto.Response
		wantErr error
	}{
		{
			name: "error uc.GetListUsers",
			mocks: func(listMock *CtrlGetListUser) {
				mocksCall := DefaultCtrlCall(listMock)
				mcks := []test_utils.MockCall{
					mocksCall[ucGetlistUserErr],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx:    context.Background(),
				params: reqres.RequestParams{},
			},
			want:    &dto.Response{},
			wantErr: errors.New("uc.GetListUsers: error"),
		},
		{
			name: "success",
			mocks: func(listMock *CtrlGetListUser) {
				mocksCall := DefaultCtrlCall(listMock)
				mcks := []test_utils.MockCall{
					mocksCall[ucGetlistUsers],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx:    context.Background(),
				params: reqres.RequestParams{},
			},
			want: &dto.Response{
				ResponseMeta: dto.ResponseMeta{
					Success:      true,
					MessageTitle: "Success",
					Message:      "list user retrieved successfully",
					ResponseTime: "0 ms.",
				},
				Data: []reqres.ResponseItem{},
			},
			wantErr: nil,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			listMock := CtrlGetListUser{
				uc: NewMockUsecaseInterface(t),
			}

			if tc.mocks != nil {
				tc.mocks(&listMock)
			}

			uc := Controller{
				uc: listMock.uc,
			}

			result, err := uc.GetListUsers(tc.args.ctx, tc.args.params)
			if err == nil {
				assert.Equal(t, tc.wantErr, err)
			} else {
				assert.Equal(t, tc.wantErr.Error(), err.Error())
			}

			if !reflect.DeepEqual(result, tc.want) {
				t.Errorf("GetListUsers() got = %v, want %v", result, tc.want)
			}

			listMock.uc.AssertExpectations(t)

		})
	}

}

func TestController_GetUserById(t *testing.T) {
	type args struct {
		ctx context.Context
		id  uint
	}
	tests := []struct {
		name    string
		mocks   func(*CtrlGetListUser)
		args    args
		want    *dto.Response
		wantErr error
	}{
		{
			name: "err record not found",
			mocks: func(user *CtrlGetListUser) {
				mocksList := DefaultCtrlCall(user)
				mcks := []test_utils.MockCall{
					mocksList[ucGetByIDErrNotFound],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx: context.Background(),
				id:  uint(1),
			},
			want:    &dto.Response{},
			wantErr: gorm.ErrRecordNotFound,
		},
		{
			name: "err default",
			mocks: func(user *CtrlGetListUser) {
				mocksList := DefaultCtrlCall(user)
				mcks := []test_utils.MockCall{
					mocksList[ucGetByIDErrDefault],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx: context.Background(),
				id:  uint(1),
			},
			want:    &dto.Response{},
			wantErr: fmt.Errorf("uc.GetUserById: error"),
		},
		{
			name: "success",
			mocks: func(user *CtrlGetListUser) {
				mocksList := DefaultCtrlCall(user)
				mcks := []test_utils.MockCall{
					mocksList[ucGetByID],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx: context.Background(),
				id:  uint(1),
			},
			want: &dto.Response{
				ResponseMeta: dto.ResponseMeta{
					Success:      true,
					MessageTitle: "Success",
					Message:      "user retrieved successfully",
					ResponseTime: "0 ms.",
				},
				Data: Item{
					ID: 1,
				},
			},
			wantErr: nil,
		},
	}
	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			listMocks := &CtrlGetListUser{
				uc: NewMockUsecaseInterface(t),
			}

			if tc.mocks != nil {
				tc.mocks(listMocks)
			}

			c := Controller{
				uc: listMocks.uc,
			}
			result, err := c.GetUserById(tc.args.ctx, tc.args.id)
			if err == nil {
				assert.Equal(t, tc.wantErr, err)
			} else {
				assert.Equal(t, tc.wantErr.Error(), err.Error())
			}

			if !reflect.DeepEqual(result, tc.want) {
				t.Errorf("GetUserById() got = %v, want %v", result, tc.want)
			}

			listMocks.uc.AssertExpectations(t)
		})
	}
}

func TestController_CreateUser(t *testing.T) {
	type args struct {
		ctx     context.Context
		payload CreateUserRequest
	}
	tests := []struct {
		name    string
		mocks   func(mocks *CtrlGetListUser)
		args    args
		want    *dto.Response
		wantErr error
	}{
		{
			name: "success",
			mocks: func(mocks *CtrlGetListUser) {
				mocksCall := DefaultCtrlCall(mocks)
				mcks := []test_utils.MockCall{
					mocksCall[ucCreateUser],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx:     context.Background(),
				payload: CreateUserRequest{},
			},
			want: &dto.Response{
				ResponseMeta: dto.ResponseMeta{
					Success:      true,
					Message:      "user has been created successfully",
					MessageTitle: "Success",
					ResponseTime: "0.ms",
				},
				Data: &entities.Users{},
			},
			wantErr: nil,
		},
		{
			name: "error uc.CreateUser",
			mocks: func(mocks *CtrlGetListUser) {
				mocksCall := DefaultCtrlCall(mocks)
				mcks := []test_utils.MockCall{
					mocksCall[ucCreateUserErr],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx:     context.Background(),
				payload: CreateUserRequest{},
			},
			want:    &dto.Response{},
			wantErr: fmt.Errorf("uc.CreateUser: error"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			listMocks := &CtrlGetListUser{
				uc: NewMockUsecaseInterface(t),
			}

			if tt.mocks != nil {
				tt.mocks(listMocks)
			}
			c := Controller{
				uc: listMocks.uc,
			}
			got, err := c.CreateUser(tt.args.ctx, tt.args.payload)
			if err == nil {
				assert.Equal(t, tt.wantErr, err)
			} else {
				assert.Equal(t, tt.wantErr.Error(), err.Error())
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateUser() got = %v, want %v", got, tt.want)

			}

			listMocks.uc.AssertExpectations(t)
		})
	}
}

func TestController_UpdateUserByID(t *testing.T) {
	type fields struct {
		uc UsecaseInterface
	}
	type args struct {
		ctx     context.Context
		payload CreateUserRequest
	}
	tests := []struct {
		name    string
		mocks   func(*CtrlGetListUser)
		args    args
		want    *dto.Response
		wantErr error
	}{
		{
			name: "success",
			mocks: func(mocks *CtrlGetListUser) {
				mocksCall := DefaultCtrlCall(mocks)
				mcks := []test_utils.MockCall{
					mocksCall[ucUpdateUserByID],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx:     context.Background(),
				payload: CreateUserRequest{},
			},
			want: dto.NewSuccessResponse(
				&entities.Users{},
				"user updated successfully",
				"0.ms",
			),
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			listMocks := &CtrlGetListUser{
				uc: NewMockUsecaseInterface(t),
			}

			if tt.mocks != nil {
				tt.mocks(listMocks)
			}

			c := Controller{
				uc: listMocks.uc,
			}
			got, err := c.UpdateUserByID(tt.args.ctx, tt.args.payload)
			if err == nil {
				assert.Equal(t, tt.wantErr, err)
			} else {
				assert.Equal(t, tt.wantErr.Error(), err.Error())
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateUserByID() got = %v, want %v", got, tt.want)

			}

			listMocks.uc.AssertExpectations(t)
		})
	}
}

func TestController_DeleteUserById(t *testing.T) {
	type fields struct {
		uc UsecaseInterface
	}
	type args struct {
		ctx context.Context
		id  uint
	}
	tests := []struct {
		name    string
		mocks   func(user *CtrlGetListUser)
		args    args
		want    *dto.Response
		wantErr error
	}{
		{
			name: "success",
			mocks: func(mocks *CtrlGetListUser) {
				mocksCall := DefaultCtrlCall(mocks)
				mcks := []test_utils.MockCall{
					mocksCall[ucDeleteUserByID],
				}
				test_utils.GenerateMockMethod(mcks)
			},
			args: args{
				ctx: context.Background(),
				id:  uint(1),
			},
			want: dto.NewSuccessResponse(
				nil,
				"user deleted successfully",
				"0.ms",
			),
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			listMocks := &CtrlGetListUser{
				uc: NewMockUsecaseInterface(t),
			}

			if tt.mocks != nil {
				tt.mocks(listMocks)
			}

			c := Controller{
				uc: listMocks.uc,
			}
			got, err := c.DeleteUserById(tt.args.ctx, tt.args.id)
			if err == nil {
				assert.Equal(t, tt.wantErr, err)
			} else {
				assert.Equal(t, tt.wantErr.Error(), err.Error())
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DeepEqual() got = %v, want %v", got, tt.want)

			}

			listMocks.uc.AssertExpectations(t)
		})
	}
}
