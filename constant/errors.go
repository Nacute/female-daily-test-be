package constant

import (
	"fmt"
)

var (
	ErrUserExist    = fmt.Errorf("user email already used")
	ErrUserNotFound = fmt.Errorf("user not found")
)

const DuplicateEntryCode = 1062
