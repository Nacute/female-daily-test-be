//go:generate mockery --all --inpackage --case snake

package repository

import (
	"context"
	"gitlab.com/Nacute/female-daily-test-be/entities"
	"gorm.io/gorm"
)

type UserRepo struct {
	db *gorm.DB
}

type UserRepoInterface interface {
	GetList(ctx context.Context) (*entities.Users, error)
	GetByID(ctx context.Context, id uint) (entities.Users, error)
	StoreDatas(
		ctx context.Context,
		users []entities.Users,
	) ([]entities.Users, error)
	Store(ctx context.Context, user *entities.Users) (*entities.Users, error)
	IsUserExist(ctx context.Context, id uint) (bool, error)
	Update(ctx context.Context, user *entities.Users) (*entities.Users, error)
	DeleteByID(ctx context.Context, id uint) error
}

func (repo UserRepo) DeleteByID(ctx context.Context, id uint) error {
	err := repo.db.WithContext(ctx).
		Delete(&entities.Users{}, id).
		Error

	return err
}

func (repo UserRepo) Update(ctx context.Context, user *entities.Users) (*entities.Users, error) {
	err := repo.db.WithContext(ctx).
		Updates(&user).
		Error
	return user, err
}

func (repo UserRepo) IsUserExist(ctx context.Context, id uint) (bool, error) {
	var found bool
	err := repo.db.WithContext(ctx).
		Model(&entities.Users{}).
		Select("COUNT(id) > 0 as `found`").
		Where("id = ?", id).
		Find(&found).Error

	return found, err
}

func (repo UserRepo) Store(ctx context.Context, user *entities.Users) (*entities.Users, error) {
	err := repo.db.WithContext(ctx).
		Create(&user).
		Error
	return user, err
}

func NewUserRepo(db *gorm.DB) UserRepoInterface {
	return &UserRepo{
		db: db,
	}
}

func (repo UserRepo) GetByID(ctx context.Context, id uint) (entities.Users, error) {
	var user entities.Users
	err := repo.db.WithContext(ctx).
		First(&user, id).Error

	return user, err
}

func (repo UserRepo) StoreDatas(
	ctx context.Context,
	users []entities.Users,
) ([]entities.Users, error) {
	var err error
	if len(users) > 0 {
		err = repo.db.
			WithContext(ctx).
			Session(&gorm.Session{
				CreateBatchSize: 500,
			}).Save(&users).Error
	}

	return users, err
}

func (repo UserRepo) GetList(ctx context.Context) (*entities.Users, error) {
	var users *entities.Users
	err := repo.db.
		WithContext(ctx).
		Find(&users).
		Error

	return users, err
}
