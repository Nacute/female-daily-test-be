package repository

import (
	"context"
	"fmt"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Nacute/female-daily-test-be/entities"
	"gitlab.com/Nacute/female-daily-test-be/utils/test_utils"
	"gorm.io/gorm"
	"regexp"
	"testing"
)

func TestUserRepo_StoreDatas(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		ctx   context.Context
		users []entities.Users
	}

	mockQuery, db, ok := test_utils.NewQueryDBMock()
	if !ok {
		return
	}

	argument := test_utils.StructToSliceOfArgs(
		entities.Users{
			ID: 1,
		},
		[]string{
			"CreatedAt",
			"UpdatedAt",
		},
	)
	argument = append(argument[1:], argument[0])

	query := "INSERT INTO `users` (`first_name`,`last_name`,`email`,`avatar`,`deleted_at`,`id`) VALUES (?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `first_name`=VALUES(`first_name`),`last_name`=VALUES(`last_name`),`email`=VALUES(`email`),`avatar`=VALUES(`avatar`),`deleted_at`=VALUES(`deleted_at`)"
	mockQuery.ExpectExec(regexp.QuoteMeta(query)).
		WithArgs(
			argument...,
		).
		WillReturnResult(sqlmock.NewResult(1, 0))

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []entities.Users
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "success no row affected",
			fields: fields{
				db: db,
			},
			args: args{
				ctx: context.Background(),
				users: []entities.Users{
					{ID: 1},
				},
			},
			want: []entities.Users{{ID: 1}},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Nil(t, err, i)
			},
		},
		{
			name: "success",
			fields: fields{
				db: db,
			},
			args: args{
				ctx:   context.Background(),
				users: []entities.Users{},
			},
			want: []entities.Users{},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Nil(t, err, i)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := UserRepo{
				db: tt.fields.db,
			}
			got, err := repo.StoreDatas(tt.args.ctx, tt.args.users)
			if !tt.wantErr(t, err, fmt.Sprintf("StoreDatas(%v, %v)", tt.args.ctx, tt.args.users)) {
				return
			}
			assert.Equal(t, tt.want, got, fmt.Sprintf("StoreDatas(%v, %v)", tt.args.ctx, tt.args.users))
		})
	}
}

func TestUserRepo_GetByID(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		ctx context.Context
		id  uint
	}
	mockQuery, db, ok := test_utils.NewQueryDBMock()
	if !ok {
		return
	}
	query := "SELECT * FROM `users` WHERE `users`.`id` = ? AND `users`.`deleted_at` IS NULL ORDER BY `users`.`id` LIMIT 1"
	mockQuery.ExpectQuery(regexp.QuoteMeta(query)).
		WithArgs(uint(1)).
		WillReturnRows(
			sqlmock.NewRows([]string{"id"}).
				AddRow(1),
		)

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entities.Users
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "success",
			fields: fields{
				db: db,
			},
			args: args{
				ctx: context.Background(),
				id:  1,
			},
			want: entities.Users{
				ID: 1,
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Nil(t, err, i)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := UserRepo{
				db: tt.fields.db,
			}
			got, err := repo.GetByID(tt.args.ctx, tt.args.id)
			if !tt.wantErr(t, err, fmt.Sprintf("GetByID(%v, %v)", tt.args.ctx, tt.args.id)) {
				return
			}
			assert.Equalf(t, tt.want, got, "GetByID(%v, %v)", tt.args.ctx, tt.args.id)
		})
	}
}

func TestUserRepo_Store(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		ctx  context.Context
		user *entities.Users
	}
	mockQuery, db, ok := test_utils.NewQueryDBMock()
	if !ok {
		return
	}
	//query :=
	mockQuery.ExpectExec("INSERT INTO `users`").
		WillReturnResult(sqlmock.NewResult(1, 1))

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *entities.Users
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "success",
			fields: fields{
				db: db,
			},
			args: args{
				ctx:  context.Background(),
				user: &entities.Users{ID: 1},
			},
			want: &entities.Users{ID: 1},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Nil(t, err, i)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := UserRepo{
				db: tt.fields.db,
			}
			got, err := repo.Store(tt.args.ctx, tt.args.user)
			if !tt.wantErr(t, err, fmt.Sprintf("Store(%v, %v)", tt.args.ctx, tt.args.user)) {
				return
			}
			assert.Equalf(t, tt.want, got, "Store(%v, %v)", tt.args.ctx, tt.args.user)
		})
	}
}

func TestUserRepo_IsUserExist(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		ctx context.Context
		id  uint
	}
	mockQuery, db, ok := test_utils.NewQueryDBMock()
	if !ok {
		return
	}

	mockQuery.ExpectQuery(regexp.QuoteMeta("SELECT COUNT(id) > 0 as `found` FROM `users` WHERE id = ? AND `users`.`deleted_at` IS NULL")).
		WithArgs(1).
		WillReturnRows(sqlmock.NewRows([]string{"found"}).AddRow(true))

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    bool
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "success",
			fields: fields{
				db: db,
			},
			args: args{
				ctx: context.Background(),
				id:  1,
			},
			want: true,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Nil(t, err, i)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := UserRepo{
				db: tt.fields.db,
			}
			got, err := repo.IsUserExist(tt.args.ctx, tt.args.id)
			if !tt.wantErr(t, err, fmt.Sprintf("IsUserExist(%v, %v)", tt.args.ctx, tt.args.id)) {
				return
			}
			assert.Equalf(t, tt.want, got, "IsUserExist(%v, %v)", tt.args.ctx, tt.args.id)
		})
	}
}

func TestUserRepo_Update(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		ctx  context.Context
		user *entities.Users
	}

	mockQuery, db, ok := test_utils.NewQueryDBMock()
	if !ok {
		return
	}

	mockQuery.ExpectExec(regexp.QuoteMeta("UPDATE `users` SET `updated_at`=? WHERE `users`.`deleted_at` IS NULL AND `id` = ?")).
		WillReturnResult(sqlmock.NewResult(1, 1))

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *entities.Users
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "success",
			fields: fields{
				db: db,
			},
			args: args{
				ctx:  context.Background(),
				user: &entities.Users{ID: 1},
			},
			want: &entities.Users{ID: 1},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Nil(t, err, i)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := UserRepo{
				db: tt.fields.db,
			}
			got, err := repo.Update(tt.args.ctx, tt.args.user)
			if !tt.wantErr(t, err, fmt.Sprintf("Update(%v, %v)", tt.args.ctx, tt.args.user)) {
				return
			}
			assert.Equalf(t, tt.want, got, "Update(%v, %v)", tt.args.ctx, tt.args.user)
		})
	}
}

func TestUserRepo_DeleteByID(t *testing.T) {
	type fields struct {
		db *gorm.DB
	}
	type args struct {
		ctx context.Context
		id  uint
	}

	mockQuery, db, ok := test_utils.NewQueryDBMock()
	if !ok {
		return
	}

	mockQuery.ExpectExec(regexp.QuoteMeta("UPDATE `users` SET `deleted_at`=? WHERE `users`.`id` = ? AND `users`.`deleted_at` IS NULL")).
		WillReturnResult(sqlmock.NewResult(0, 1))

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "success",
			fields: fields{
				db: db,
			},
			args: args{
				ctx: context.Background(),
				id:  1,
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.Nil(t, err, i)
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := UserRepo{
				db: tt.fields.db,
			}
			tt.wantErr(t, repo.DeleteByID(tt.args.ctx, tt.args.id), fmt.Sprintf("DeleteByID(%v, %v)", tt.args.ctx, tt.args.id))
		})
	}
}
