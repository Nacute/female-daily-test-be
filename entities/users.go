package entities

import (
	"gorm.io/gorm"
	"time"
)

type Users struct {
	ID        uint           `gorm:"type:BIGINT UNSIGNED NOT_NULL AUTO_INCREMENT;primaryKey" json:"id"`
	FirstName string         `gorm:"column:first_name;type:VARCHAR(100)" json:"firstName"`
	LastName  string         `gorm:"column:last_name;type:VARCHAR(100)" json:"lastName"`
	Email     string         `gorm:"type:VARCHAR(80) NOT_NULL" json:"email"`
	Avatar    string         `gorm:"type:VARCHAR(255)" json:"avatar"`
	CreatedAt time.Time      `gorm:"column:created_at;type:TIMESTAMP;default:CURRENT_TIMESTAMP;" json:"createdAt"`
	UpdatedAt time.Time      `gorm:"column:updated_at;type:TIMESTAMP;default:CURRENT_TIMESTAMP;" json:"updatedAt"`
	DeletedAt gorm.DeletedAt `json:"deletedAt"`
}

func (u Users) TableName() string {
	return "users"
}
