package dto

type Response struct {
	ResponseMeta
	Data any `json:"data"`
}

type ResponseMeta struct {
	Success      bool   `json:"success"`
	MessageTitle string `json:"messageTitle"`
	Message      string `json:"message"`
	ResponseTime string `json:"responseTime"`
}

func NewSuccessResponse(data any, msg string, resTime string) *Response {
	return &Response{
		ResponseMeta: ResponseMeta{
			Success:      true,
			MessageTitle: "Success",
			Message:      msg,
			ResponseTime: resTime,
		},
		Data: data,
	}
}

func DefaultInvalidInputResponse(errs map[string][]string) *Response {
	return &Response{
		ResponseMeta: ResponseMeta{
			Success:      false,
			MessageTitle: "invalid data",
		},
		Data: errs,
	}
}

func DefaultErrorResponse(msg string, resTime string) *Response {
	return &Response{
		ResponseMeta: ResponseMeta{
			Success:      false,
			MessageTitle: "Oops, something went wrong.",
			Message:      msg,
			ResponseTime: resTime,
		},
	}
}
