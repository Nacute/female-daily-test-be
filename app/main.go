package main

import (
	"fmt"
	_ "github.com/joho/godotenv/autoload"
	"gitlab.com/Nacute/female-daily-test-be/utils/api"
)

func main() {

	app := api.Default()

	err := app.Start()
	if err != nil {
		panic(fmt.Sprintf("panic with error: %s", err.Error()))
	}

}
