//go:generate mockery --all --inpackage --case snake

package reqres

import (
	"context"
	"fmt"
	"gitlab.com/Nacute/female-daily-test-be/utils/inetproto"
	"net/http"
	"os"
)

type Reqres struct {
	http inetproto.HttpInterface
}

type Services interface {
	GetListUser(
		ctx context.Context,
		params RequestParams,
	) (GetListUsereResponse, error)
}

func (r Reqres) GetListUser(
	ctx context.Context,
	params RequestParams,
) (GetListUsereResponse, error) {
	url := os.Getenv("REQRES_URL") + fmt.Sprintf("?page=%s", params.Page)
	if params.PerPage != "" {
		url = url + fmt.Sprintf("&per_page=%s", params.PerPage)
	}
	req, err := r.createRequest(
		ctx,
		http.MethodGet,
		url,
		nil,
	)
	if err != nil {
		return GetListUsereResponse{}, err
	}

	res, err := r.http.Do(req)
	if err != nil {
		return GetListUsereResponse{}, fmt.Errorf("http.Do: %w", err)
	}

	if res.StatusCode != http.StatusOK {
		return GetListUsereResponse{}, fmt.Errorf("error from reqres server")
	}
	var response GetListUsereResponse
	err = r.http.BindResponse(res, &response)
	if err != nil {
		return GetListUsereResponse{}, fmt.Errorf("http.BindResponse: %w", err)
	}

	return response, nil
}

func (r Reqres) createRequest(
	ctx context.Context,
	method string,
	url string,
	reqBody any,
) (*http.Request, error) {
	body := r.http.ReadRequest(reqBody)
	req, err := r.http.NewRequestWithContext(
		ctx,
		method,
		url,
		body,
	)
	if err != nil {
		return nil, fmt.Errorf("http.NewRequestWithContext: %w", err)
	}

	req.Header.Set("accept", "application/json")

	return req, nil
}

func New(client inetproto.Http) Services {
	return &Reqres{
		http: client,
	}
}
