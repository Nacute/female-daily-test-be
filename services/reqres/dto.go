package reqres

type RequestParams struct {
	Page    string `json:"page"`
	PerPage string `json:"perPage"`
}

type ResponseItem struct {
	Id        int    `json:"id"`
	Email     string `json:"email"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Avatar    string `json:"avatar"`
}

type SupportItem struct {
	Url  string `json:"url"`
	Text string `json:"text"`
}

type GetListUsereResponse struct {
	Page       int            `json:"page"`
	PerPage    int            `json:"per_page"`
	Total      int            `json:"total"`
	TotalPages int            `json:"total_pages"`
	Data       []ResponseItem `json:"data"`
	Support    SupportItem    `json:"support"`
}
