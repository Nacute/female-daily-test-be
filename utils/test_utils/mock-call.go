//go:generate mockery --all --inpackage --case snake

package test_utils

import (
	"bytes"
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/mock"
	"golang.org/x/exp/slices"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"net/http"
	"reflect"
	"testing"
	"time"
)

type MockCall struct {
	Mock    *mock.Mock
	Method  string
	Args    []any
	Result  []any
	Times   int
	exclude bool
}

func GenerateMockMethod(mck []MockCall) {
	for i, _ := range mck {
		mck[i].Mock.On(mck[i].Method, mck[i].Args...).
			Return(mck[i].Result...).Times(mck[i].Times)
	}
}

func NewQueryDBMock() (sqlmock.Sqlmock, *gorm.DB, bool) {
	mockConn, mockQuery, err := sqlmock.New()
	if err != nil {
		log.Println(err)
		return nil, nil, false
	}
	mockDB, err := GormMysqlFromConnMock(mockConn)
	if err != nil {
		log.Println(err)
		return nil, nil, false
	}
	return mockQuery, mockDB, true
}

func GormMysqlFromConnMock(db *sql.DB) (*gorm.DB, error) {
	mysqlConfig := mysql.Config{
		Conn:                      db,
		SkipInitializeWithVersion: true,
	}
	options := &gorm.Config{
		SkipDefaultTransaction: true,
		NowFunc: func() time.Time {
			return time.Time{}
		},
	}

	return gorm.Open(mysql.New(mysqlConfig), options)
}

func StructToSliceOfArgs[T any](data T, exclude []string) []driver.Value {
	t := reflect.TypeOf(data)
	length := t.NumField()
	var args []driver.Value
	for i := 0; i < length; i++ {
		fieldName := t.Field(i).Name
		if !slices.Contains(exclude, fieldName) {
			value := reflect.ValueOf(data).Field(i)
			args = append(args, value.Interface())
		}

	}

	return args
}

func GenerateMockRequest(t *testing.T, body any, method string, path string) *http.Request {
	bodyByte, err := json.Marshal(body)
	if err != nil {
		log.Println(fmt.Errorf("json.Marshal: %w", err))
		t.Fail()
		return &http.Request{}
	}
	req, err := http.NewRequest(method, path, bytes.NewReader(bodyByte))
	if err != nil {
		log.Println(fmt.Errorf("new request: %w", err))
		t.Fail()
		return &http.Request{}
	}
	return req
}
