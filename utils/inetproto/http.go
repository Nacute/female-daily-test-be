package inetproto

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

type Http struct {
	client *http.Client
}

func Default() Http {
	return Http{
		client: http.DefaultClient,
	}
}

type HttpInterface interface {
	ReadRequest(req any) io.Reader
	NewRequestWithContext(ctx context.Context, method, url string, body io.Reader) (*http.Request, error)
	Do(req *http.Request) (*http.Response, error)
	BindResponse(res *http.Response, payload any) error
}

func (h Http) ReadRequest(req any) io.Reader {
	payloadBytes, err := json.Marshal(req)
	op := "utils.http.ReadRequest:"
	if err != nil {
		log.Println(op, err)
		return nil
	}
	return bytes.NewBuffer(payloadBytes)
}

func (h Http) NewRequestWithContext(ctx context.Context, method, url string, body io.Reader) (*http.Request, error) {
	return http.NewRequestWithContext(ctx, method, url, body)
}

func (h Http) Do(req *http.Request) (*http.Response, error) {
	return h.client.Do(req)
}

func (h Http) BindResponse(res *http.Response, payload any) error {
	body, err := io.ReadAll(res.Body)
	op := "utils.http.BindResponse:"
	if err != nil {
		log.Println(op, err)
		return err
	}

	fmt.Println(string(body))
	err = json.Unmarshal(body, payload)
	if err != nil {
		log.Println(op, err)
		return err
	}

	return nil
}
