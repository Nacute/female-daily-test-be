package api

import "github.com/gin-gonic/gin"

type Api struct {
	engine  *gin.Engine
	routers []Router
}

type Router interface {
	Route(handler *gin.RouterGroup)
}

func (api Api) Start() error {
	root := api.engine.Group("/main")

	for _, router := range api.routers {
		router.Route(root)
	}

	err := api.engine.Run("0.0.0.0:8080")
	if err != nil {
		return err
	}

	return nil
}
