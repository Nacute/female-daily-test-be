package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/Nacute/female-daily-test-be/middleware"
	"gitlab.com/Nacute/female-daily-test-be/module/user"
	"gitlab.com/Nacute/female-daily-test-be/repository"
	"gitlab.com/Nacute/female-daily-test-be/services/reqres"
	"gitlab.com/Nacute/female-daily-test-be/utils/db"
	"gitlab.com/Nacute/female-daily-test-be/utils/inetproto"
)

func Default() *Api {
	server := gin.Default()

	dbConn, err := db.Default()
	if err != nil {
		panic(fmt.Sprintf("panic at db connection: %s", err.Error()))
	}

	defaultClient := inetproto.Default()
	defaultEnigma := middleware.NewEnigma()
	newReqres := reqres.New(defaultClient)
	userRepo := repository.NewUserRepo(dbConn)

	var routers = []Router{
		user.NewRouter(newReqres, userRepo, defaultEnigma),
	}

	return &Api{
		engine:  server,
		routers: routers,
	}

}
