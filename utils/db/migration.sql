create table users
(
    id         bigint unsigned auto_increment
        primary key,
    first_name varchar(100)                        null,
    last_name  varchar(100)                        null,
    email      varchar(80)                         not null comment 'lenght 80 char because its standar form gmail provider',
    avatar     varchar(255)                        null,
    created_at timestamp default CURRENT_TIMESTAMP null,
    updated_at timestamp default CURRENT_TIMESTAMP null,
    deleted_at timestamp default CURRENT_TIMESTAMP null,
    constraint users_pk_email
        unique (email)
);

